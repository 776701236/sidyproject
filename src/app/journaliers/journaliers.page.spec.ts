import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JournaliersPage } from './journaliers.page';

describe('JournaliersPage', () => {
  let component: JournaliersPage;
  let fixture: ComponentFixture<JournaliersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JournaliersPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JournaliersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
