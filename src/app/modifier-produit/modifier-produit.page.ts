import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-modifier-produit',
  templateUrl: './modifier-produit.page.html',
  styleUrls: ['./modifier-produit.page.scss'],
})
export class ModifierProduitPage implements OnInit {


  id = null;
  Designation = "";
  Prix = "";
   

  constructor(public  publcService:PublicService, private router:Router, private activatedRoute: ActivatedRoute ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.Designation = this.activatedRoute.snapshot.paramMap.get('Designation');
    this.Prix = this.activatedRoute.snapshot.paramMap.get('Prix');
    console.log(this.Designation)
    localStorage.setItem('idVente', this.id)

    /*
    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
    console.log(this.id)
    localStorage.setItem('idVente', this.id)
    */
   
  }

  // localStorage.setItem('userId', this.id)


  onModif(vente){ 
    var id = localStorage.getItem('userId'); 
       this.publcService.updateVente({
         Designation: vente.Designation,
         Prix: vente.Prix,
         id_userv: id,
       })
       .then(data =>{
         console.log(data);
         if(data){
          this.router.navigateByUrl('/menu/acceuil');
          console.log('update réussi')
        }else{
          this.router.navigateByUrl('/ajoutvente');
          console.log('échec') 
        }
      }, err => {
         console.log(err);
      });
      console.log(vente);
  }

 

  // this.router.navigateByUrl('/menu/acceuil/menu/acceuil/vente');

  deleteVente(id){
    this.publcService.deleteVente(id).then(()=>{
      // this.VentePage.onLoadVente();
      this.router.navigateByUrl('/menu/acceuil');
    })
  }



}
