import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-journal',
  templateUrl: './journal.page.html',
  styleUrls: ['./journal.page.scss'],
})
export class JournalPage implements OnInit {

  myDate = new Date().toISOString();

  totalv = '';
  totald = ''; 

  public imz = {
    logo: "assets/images/journal.jpg",
    logo1: "assets/images/journal1.jpg",
    gif: "assets/images/giphy.gif"
  }

  constructor() { }

  ngOnInit() {
   this.doRefresh(event);
  }


  doRefresh(event) {
    console.log('Begin async operation');
    var tab = this.myDate.split('-');
    console.log(tab)
    let date = {
      annee : tab[0],
      mois : tab[1],
      jour : tab[2].split('T')[0],
    }
     
    console.log(date)

    this.totalv = localStorage.getItem('totalVenteDay');
    console.log(this.totalv);
    
    this.totald = localStorage.getItem('totalDepenseDay');;
    console.log(this.totald);
    //event.target.complete();
  }

  dateChanged(date){
    console.log(date.detail.value)
    console.log(this.myDate)
  }

}
/*
  var tab = this.myDate.split('-');
    console.log(tab)
     var annee = tab[0];
     var mois = tab[1];
     var jour = tab[2].split('T')[0];
     console.log(jour);
 */