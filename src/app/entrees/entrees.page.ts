import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';
 
@Component({
  selector: 'app-entrees',
  templateUrl: './entrees.page.html',
  styleUrls: ['./entrees.page.scss'],
})
export class EntreesPage implements OnInit {

  constructor(private publc:PublicService) { }

  result : any ;

  ngOnInit() {
    this.onLoadEntree();
  }

  onLoadEntree() {
    this.publc.getEntree()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  

    });
  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getEntree()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  
    
    });
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

}
 
