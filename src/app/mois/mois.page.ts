import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-mois',
  templateUrl: './mois.page.html',
  styleUrls: ['./mois.page.scss'],
})
export class MoisPage{
  currentDate;
  formatteDate;
  FormatteMonth;
  mois :string;

  moIs =[];
  
  constructor(public NavCtrl: NavController, private publc:PublicService) { 
    this.currentDate = new Date();
    this.getFormatteDate()
    this.getFormatteMonth()
    this.doRefresh(event);
 }
 result : any ;
  
 getFormatteDate(){
  var dateobj = new Date()
  dateobj.setDate(dateobj.getDate() - 1);
  console.log(dateobj);
  this.formatteDate = dateobj;
  console.log(this.formatteDate);
 }



 getFormatteMonth(){
  var tab = [
    {title:"Janvier",positions:1 ,position:0}, 
    {title:"Fevrier" ,positions:2 ,position:1 },
    {title:"Mars" ,positions:3 ,position:2},
    {title:"Avril" ,positions:4 ,position:3} ,
    {title:"Mai" ,positions:5,position:4},
    {title:"Juin" ,positions:6,position:5},
    {title:"Juillet" ,positions:7, position:6},
    {title:"Aout" ,positions:8, position:7},
    {title:"Septembre" ,positions:9, position:8},
    {title:"Octobre" ,positions:10, position:9},
    {title:"Novembre" ,positions:11, position:10},
    {title:"Decembre" ,positions:12, position:11},
  ]
   
  var newDate=new Date()
  newDate.setMonth(newDate.getUTCMonth());
  for(let i=newDate.getUTCMonth(); i>=0; i--){
  console.log(i)
  this.moIs.push(tab[i]);
}
}

 doRefresh(event) {
  console.log('Begin async operation');

  this.publc.getVente()
  .then(data=>{
   
   this.result = JSON.parse(data.data);
   console.log(this.result.status) 

  });

  setTimeout(() => {
    console.log('Async operation has ended');
    event.target.complete();
  }, 2000);
}
}