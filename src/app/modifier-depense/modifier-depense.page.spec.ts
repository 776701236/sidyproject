import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierDepensePage } from './modifier-depense.page';

describe('ModifierDepensePage', () => {
  let component: ModifierDepensePage;
  let fixture: ComponentFixture<ModifierDepensePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierDepensePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierDepensePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
