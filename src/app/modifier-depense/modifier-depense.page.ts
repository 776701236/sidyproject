import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-modifier-depense',
  templateUrl: './modifier-depense.page.html',
  styleUrls: ['./modifier-depense.page.scss'],
})
export class ModifierDepensePage implements OnInit {

  id = null;
  Designation = "";
  Prix = "";

  constructor(public  publcService:PublicService, private router:Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.Designation = this.activatedRoute.snapshot.paramMap.get('Designation');
    this.Prix = this.activatedRoute.snapshot.paramMap.get('Prix');
    console.log(this.Designation)
    localStorage.setItem('idDepense', this.id)
  }

  onModif(depense){ 
    var id = localStorage.getItem('userId'); 
       this.publcService.updateDepense({
         Designation: depense.Designation,
         Prix: depense.Prix,
         id_userv: id,
       })
       .then(data =>{
         console.log(data);
         if(data){
          this.router.navigateByUrl('/menu/acceuil/menu/acceuil/depense');
          console.log('ajout réussi')
        }else{
          this.router.navigateByUrl('ajoutdepense');
          console.log('échec') 
        }
      }, err => {
         console.log(err);
      });
      console.log(depense);
  }

  deleteDepense(id){
    this.publcService.deleteDepense(id).then(()=>{
      this.router.navigateByUrl('/menu/acceuil/menu/acceuil/depense');
    })
  }

}
