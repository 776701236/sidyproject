import { Injectable } from '@angular/core';
import {rootUrl} from "../../app/apiurls/serverurls.js";
import { HTTP } from '@ionic-native/http/ngx';


@Injectable()
export class AuthenticateService { 

  constructor( private httpp:HTTP ) {

   }

   createUser(user){
    return this.httpp.post(rootUrl+'/api/registeruser/', user, {});
  }

   login(user){
    return this.httpp.post(rootUrl+'/api/loginuser/', user, {});
  }
 
  logout() {
    localStorage.removeItem('userId');
  }

 }




