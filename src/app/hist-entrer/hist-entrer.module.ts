import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistEntrerPage } from './hist-entrer.page';


const routes: Routes = [
  {
    path: 'menu/hist-entrer',
    component: HistEntrerPage,
    children: [
      {
        path: 'journaliers',
        children: [
          {
            path: '',
            loadChildren: '../journaliers/journaliers.module#JournaliersPageModule'
          }
        ]
              },
      {
        path: 'mois',
        children: [
          {
            path: '',
            loadChildren: '../mois/mois.module#MoisPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: 'menu/hist-entrer/journaliers',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'menu/hist-entrer/journaliers',
    pathMatch: 'full'
}
];


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HistEntrerPage]
})
export class HistEntrerPageModule {}
