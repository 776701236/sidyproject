import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mensuel',
  templateUrl: './mensuel.page.html',
  styleUrls: ['./mensuel.page.scss'],
})
export class MensuelPage implements OnInit {

  constructor(private publc:PublicService, private router:Router) { }

  myDate = new Date().toISOString();

  result : any ;
  listMoisAct = [];
  listMoisPass = [];
  aOut = [];
  Sept = [];

  lesMois = [
  { position: 1, nom: "Janvier" }, 
  { position: 2, nom: "Fevrier" },
  { position: 3, nom: "Mars" },
  { position: 4, nom: "Avril" },
  { position: 5, nom: "Mai" },
  { position: 6, nom: "Juin" },
  { position: 7, nom: "Juillet" },
  { position: 8, nom: "Aout" },
  { position: 9, nom: "Septembre" },
  { position: 10, nom: "Octobre" },
  { position: 11, nom: "Novembre" },
  { position: 12, nom: "Decembre" },
]

janvier = {position: 1, nom: "Janvier"}
fevrier = {position: 2, nom: "Fevrier"}
mars = {position: 3, nom: "Mars"}
avril = {position: 4, nom: "Avril"}
mai = {position: 5, nom: "Mai"}
juin = {position: 6, nom: "Juin"}
juillet = {position: 7, nom: "Juillet"}
aout = {position: 8, nom: "Aout"}
septembre = {position: 9, nom: "Septembre"}
octobre = {position: 10, nom: "Octobre"}
novembre = {position: 11, nom: "Novembre"}
decembre = {position: 12, nom: "Decembre"}


  ngOnInit() {
    this.onLoadVente(); 
  }

  onLoadVente() {
  
    this.publc.getVente()
    .then(data=>{
      this.result = JSON.parse(data.data);
      
      this.listing();
      
      
    
    });
  }



  dateDay(){
    var tab = this.myDate.split('-');
    
    let date = {
      annee : tab[0],
      mois : tab[1],
      jour : tab[2].split('T')[0],
     
    }   
    return date;
  }

  listing(){
    let today = this.dateDay();

   this.result.data.forEach(produit => {
     
     let tab = produit.Date.split('-');
     let date = {
       annee : tab[0],
       mois : tab[1],
       jour : tab[2].split('T')[0],
     }
     
            /////////////// condition pour le mois en cours
     if(today.annee == date.annee && today.mois == date.mois){

      console.log("Mois en cours... ", this.listMoisAct.push(produit));

     }

    
           /////////////// condition pour mois passé 
    if(today.annee == date.annee){

     console.log("Mois passé ", this.listMoisPass.push(produit));

    }

    
          //////////////// condition pour le mois de janvier
    if(today.annee == date.annee && 1 == date.mois){

        console.log("Janvier")
        
    }

         //////////////// condition pour le mois d'Aout
    if(today.annee == date.annee && 8 == date.mois){
     
         
          console.log("Aout ", this.aOut.push(produit));
    
    }

          //////////////// condition pour le mois de Septembre
    if(today.annee == date.annee && 9 == date.mois){

            console.log("Septembre", this.Sept.push(produit));
      
    }

    

    
   });

   console.log("la valeur totale vente du jour ")
 }
  

}

/*
 onLoadVente() {
    let tab = [];

    this.publc.getVente()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
   
     for(let i= 0; i < this.result.data.length; i++){
      
      if(!tab.includes(this.result.data[i])){
        console.log('ddd')
        tab.push(this.result.data[i])
      }

     }
     console.log('sss', tab)
    },err=>{
      console.log(err); 
    });
  }
 */

 /*
    this.result = JSON.parse(data.data);

     for(let i= 0; i < this.result.data.length; i++){
      
      let tab = this.result.data[i].Date.split('-');
      let date = {
        annee : tab[0],
        mois : tab[1],
        jour : tab[2].split('T')[0],
        
      }
      console.log(date)
    
      if (tabV.indexOf(date[i]) === -1){
        tabV.push( this.result.data[i].Date)
        
      }
      
     //console.log(this.result.data[i].Date);
    

     }
 */

 /*this.result.data.forEach(produit => {

      let tab = produit.Date.split('-');
      let date = {
        annee : tab[0],
        mois : tab[1],
        jour : tab[2].split('T')[0],
      }

      if(!date){

      }

      console.log(date)
    });*/
    
