import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-modifier-pret',
  templateUrl: './modifier-pret.page.html',
  styleUrls: ['./modifier-pret.page.scss'],
})
export class ModifierPretPage implements OnInit {

  id = null;
  Client = "";
  Telephone = "";
  Designation = "";
  Montant = "";

  constructor(public  publcService:PublicService, private router:Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.Client = this.activatedRoute.snapshot.paramMap.get('Client');
    this.Telephone = this.activatedRoute.snapshot.paramMap.get('Telephone');
    this.Designation = this.activatedRoute.snapshot.paramMap.get('Designation');
    this.Montant = this.activatedRoute.snapshot.paramMap.get('Montant');
    console.log(this.Designation)
    localStorage.setItem('idPret', this.id)
  }

  onModif(pret){ 
    if(pret.prete == "Remboursement"){
      var id = localStorage.getItem('userId');
      this.publcService.updatePret({
      Client: pret.Nom_Client,
      Designation: pret.Libelle,
      Montant: pret.Montant,
      Telephone: pret.Phone,
      id_userp: id,
      })
      .then(data =>{
        console.log(data);
        if(data){
         this.router.navigateByUrl('/menu/acceuil/menu/acceuil/pret');
         console.log('pret')
       }else{
         this.router.navigateByUrl('modifier-pret');
         console.log('échec') 
       }
     }, err => {
        console.log(err);
     });
      }else{
        var id = localStorage.getItem('userId');
      this.publcService.updatePret({
        Client: pret.Nom_Client,
        Designation: pret.Libelle,
        Montant: - pret.Montant,
        Telephone: pret.Phone,
        id_userp: id,
      })
      .then(data =>{
        console.log(data);
        if(data){
         this.router.navigateByUrl('menu/acceuil/menu/acceuil/pret');
         console.log('ajout réussi')
       }else{
         this.router.navigateByUrl('modifier-pret');
         console.log('échec') 
       }
     }, err => {
        console.log(err);
     });
      }
  }

  deletePret(id){
    this.publcService.deletePret(id).then(()=>{
      this.router.navigateByUrl('/menu/acceuil/menu/acceuil/pret');
    })
  }

}
