import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierPretPage } from './modifier-pret.page';

describe('ModifierPretPage', () => {
  let component: ModifierPretPage;
  let fixture: ComponentFixture<ModifierPretPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierPretPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierPretPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
