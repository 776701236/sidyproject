import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';
import { IonInfiniteScroll } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vente',
  templateUrl: './vente.page.html',
  styleUrls: ['./vente.page.scss'],
})
export class VentePage implements OnInit {

  myDate = new Date().toISOString();

  constructor(private publc:PublicService, private router:Router) { }

  result : any ;
  somming : any;
  date: any;
  sommeDay: any;
  sommeMoi : any;

  ngOnInit() {
    
    this.doRefresh(event);
  } 

  

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getVente()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result);
 
     // console.log(this.result.data[0].Prix);
      

     var returnedVal = this.sum([1,2,3,4,5]);
     console.log(returnedVal);

     this.somming = this.som();
     console.log(this.somming);
     
      //console.log(this.result.data[0].Date)
      
      this.sommeToday();

      this.sommeMois();
      
    });
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  dateDay(){
    var tab = this.myDate.split('-');
    
    let date = {
      annee : tab[0],
      mois : tab[1],
      jour : tab[2].split('T')[0],
    }
     
    return date;
  }

  som(){
    let s = 0;
    for (let i=0; i < this.result.data.length; i++) {
      s = s + this.result.data[i].Prix;
     
    }
    return s;
   }

   sommeToday(){
     let today = this.dateDay();
     let somm = 0;

    this.result.data.forEach(produit => {
      
      let tab = produit.Date.split('-');
      let date = {
        annee : tab[0],
        mois : tab[1],
        jour : tab[2].split('T')[0],
      }
      
      if(today.annee == date.annee && today.jour == date.jour && today.mois == date.mois){
        
        somm = somm + produit.Prix;
        this.sommeDay = somm;
        localStorage.setItem('totalVenteDay', this.sommeDay)
      }
    });

    console.log("la valeur totale vente du jour ", this.sommeDay)
  }

  sommeMois(){
    let today = this.dateDay();
    let somm = 0;

   this.result.data.forEach(produit => {
     
     let tab = produit.Date.split('-');
     let date = {
       annee : tab[0],
       mois : tab[1],
       jour : tab[2].split('T')[0],
     }
     
     if(today.annee == date.annee && today.mois == date.mois){
       
       somm = somm + produit.Prix;
       this.sommeMoi = somm;
       localStorage.setItem('totalVenteMois', this.sommeMoi);
     }
   });

   console.log("la valeur totale vente du mois ", this.sommeMoi)
 }

   
   sum(arr){
    let s = 0; 
    for (let i=0; i < 5; i++) {
     s = s + arr[i];
     }
     return s;



   }
  
   
   
  
/*
  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getVente()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.count);
    ////////////
    //let s = 0;
    //for (let i=0; i < this.result.data.length; i++) {
     // s = s + this.result.data.Prix[i];
     //console.log(this.result.data.Prix)
     //console.log(i)
    // }
    ///////////
       function sum(arr){
        let s = 0; 
        for (let i=0; i < 5; i++) {
         s = s + arr[i];
         }
         return s;
       }
       var returnedVal = sum([1,2,3,4,5]);
       console.log(returnedVal);
       ////////////
      event.target.complete();
    },err=>{
      console.log(err); 
    });
  }
*/

 deleteVente(id){
   this.publc.deleteVente(id).then(()=>{
    // this.onLoadVente();
   })
 }

 updateVente(id){
   this.publc.updateVente(id).then(()=>{
    // this.onLoadVente();
   })
 } 
  }

 

 /*
  doInfinite(IonInfiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() =>{
      for (let i = 0; i < 30; i++) {
        this.result.push(this.result.length);
      }

      console.log('Async operation has ended');
      IonInfiniteScroll.complete();
    }, 500);
  }
  */

  /*
    let s = 0;
    for (let i=0; i < this.result.data.length; i++) {
     console.log("la somme",s)
     console.log("nombre de prix",this.result.data[i].Prix)
    }
  */

  /*
  som(){
    let s = 0;
    for (let i=0; i < this.result.data.length; i++) {
      s = s + this.result.data[i].Prix;
     //console.log("nombre de prix",this.result.data[i].Prix)
    }
    return s;
   }
   */