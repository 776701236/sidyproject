import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-sorties',
  templateUrl: './sorties.page.html',
  styleUrls: ['./sorties.page.scss'],
})
export class SortiesPage implements OnInit {

  constructor(private publc:PublicService) { }

  result : any ;

  ngOnInit() {
    this.onLoadSortie();
  }

  onLoadSortie() {
   
  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getSortie()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status);
     event.target.complete();

    },err=>{
      console.log(err); 
    });
  }    

}
