import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutsortiesPage } from './ajoutsorties.page';

describe('AjoutsortiesPage', () => {
  let component: AjoutsortiesPage;
  let fixture: ComponentFixture<AjoutsortiesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutsortiesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutsortiesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
