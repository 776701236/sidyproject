import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  logout() {
    localStorage.removeItem('token'); 
    this.router.navigateByUrl('/home');
  }

}
