import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-depense',
  templateUrl: './depense.page.html',
  styleUrls: ['./depense.page.scss'],
})
export class DepensePage implements OnInit {

  myDate = new Date().toISOString();

  constructor(private publc:PublicService) { }

  result : any ;
  somming : any;
  date: any;
  sommeDay: any;
  sommeMoi: any;

  ngOnInit() {
    this.doRefresh(event);
  }

 // onLoadDepense() {
  //// 
 //// }

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getDepense()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status) 
     
     this.somming = this.som();
     console.log(this.somming);
     
     this.sommeToday();
     
     this.sommeMois();

    });

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  dateDay(){
    var tab = this.myDate.split('-');
    
    let date = {
      annee : tab[0],
      mois : tab[1],
      jour : tab[2].split('T')[0],
    }
     
    return date;
  }

  som(){
    let s = 0;
    for (let i=0; i < this.result.data.length; i++) {
      s = s + this.result.data[i].Prix;
     //console.log("nombre de prix",this.result.data[i].Prix) 
    }
    return s;
   }

   sommeToday(){
    let today = this.dateDay();
    let somm = 0;

   this.result.data.forEach(produit => {
     
     let tab = produit.Date.split('-');
     let date = {
       annee : tab[0],
       mois : tab[1],
       jour : tab[2].split('T')[0],
     }
     
     if(today.annee == date.annee && today.jour == date.jour && today.mois == date.mois){
       
       somm = somm + produit.Prix;
       this.sommeDay = somm;
       localStorage.setItem('totalDepenseDay', this.sommeDay)
     }
   });

   console.log("la valeur totale du jour ", this.sommeDay)
 }

 sommeMois(){
  let today = this.dateDay();
  let somm = 0;

 this.result.data.forEach(produit => {
   
   let tab = produit.Date.split('-');
   let date = {
     annee : tab[0],
     mois : tab[1],
     jour : tab[2].split('T')[0],
   }
   
   if(today.annee == date.annee && today.mois == date.mois){
     
     somm = somm + produit.Prix;
     this.sommeMoi = somm;
     localStorage.setItem('totalDepenseMois', this.sommeMoi);
   }
 });

 console.log("la valeur totale depense du mois ", this.sommeMoi)
}

}
