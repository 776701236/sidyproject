import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [


  {
    path: '',
    component: MenuPage,
      children: [
        { path: 'journal', loadChildren: '../journal/journal.module#JournalPageModule' },
        { path: 'hebdomadaire', loadChildren: '../hebdomadaire/hebdomadaire.module#HebdomadairePageModule' },
        { path: 'mensual', loadChildren: '../mensual/mensual.module#MensualPageModule' },
        { path: 'inventaires', loadChildren: '../inventaires/inventaires.module#InventairesPageModule' },
        { path: 'benefice', loadChildren: '../benefice/benefice.module#BeneficePageModule' },
        { path: 'acceuil', loadChildren: '../acceuil/acceuil.module#AcceuilPageModule'},
        { path: 'gestionstckok', loadChildren: '../gestionstckok/gestionstckok.module#GestionstckokPageModule' },
        { path: 'nouscontacter', loadChildren: '../nouscontacter/nouscontacter.module#NouscontacterPageModule'},
        { path: 'aprospos', loadChildren: '../aprospos/aprospos.module#AprosposPageModule'},
        { path: 'ajoutproduit', loadChildren: '../ajoutproduit/ajoutproduit.module#AjoutproduitPageModule'},
        { path: 'hist-sorti', loadChildren: '../hist-sorti/hist-sorti.module#HistSortiPageModule'},
        { path: 'hist-entrer', loadChildren: '../hist-entrer/hist-entrer.module#HistEntrerPageModule'},

      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
