import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-journaliers1',
  templateUrl: './journaliers1.page.html',
  styleUrls: ['./journaliers1.page.scss'],
})
export class Journaliers1Page {

  currentDate;
  formatteDate;
  FormatteMonth;
  mois :string;

  jour =[];
  
  constructor(public NavCtrl: NavController, private publc:PublicService) { 
    this.currentDate = new Date();
    this.getFormatteDate()
    this.getFormatteMonth()
    this.doRefresh(event);
    this.GetLastDayOfThisMonth () 
 }
 result : any ;
 getFormatteDate(){
  var dateobj = new Date()
  dateobj.setDate(dateobj.getDate() - 1);
  //console.log(dateobj);
  this.formatteDate = dateobj;
  //console.log(this.formatteDate);
 }


  getFormatteMonth(){
  
     
    var newDate=new Date()
    newDate.setMonth(newDate.getUTCMonth());
    for(let i=newDate.getUTCMonth(); i>=0; i--){
    //console.log(i)
   
  }
  }

  doRefresh(event) {
    //console.log('Begin async operation');
  
    this.publc.getVente()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     //console.log(this.result.status) 
  
    });
  
    setTimeout(() => {
      //console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  GetLastDayOfThisMonth () {
    var tab = [
      {title:"Dimanche", position:0},
      {title:"Lundi", position:1},
      {title:"Mardi", position:2},
      {title: "Mercredi", position:3},
      {title:"Jeudi", position:4},
      {title:"Vendredi", position:5},
      {title:"Samedi", position:6},
      
    ]
   var myDate = new Date ();
     myDate.setMonth(myDate.getUTCMonth());
     var myMonth = myDate.setMonth (myDate.getMonth() + 1); 
     var theDay = myDate.setDate (0); 
     var lastDay = myDate.getDate ()
     console.log(lastDay);
     for(let i=0; i<=lastDay; i++){
      this.jour.push(tab[i]);
      console.log(i)
     var jours = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
     var aujourdhui = new Date();
     var result = jours[aujourdhui.getDay()] + " " + aujourdhui.getDate() + "/" + aujourdhui.getMonth() + "/" + aujourdhui.getFullYear();
     console.log(result)
    }
  
}
data = [
  {
      notes: 'Game was played',
      time: '2017-10-04T20:24:30+00:00',
      sport: 'hockey',
      owner: 'steve',
      players: '10',
      game_id: 1
  },
  {
      notes: 'Game was played',
      time: '2017-10-04T12:35:30+00:00',
      sport: 'lacrosse',
      owner: 'steve',
      players: '6',
      game_id: 2
  },
  {
      notes: 'Game was played',
      time: '2017-10-14T20:32:30+00:00',
      sport: 'hockey',
      owner: 'steve',
      players: '4',
      game_id: 3
  },
  {
      notes: 'Game was played',
      time: '2017-10-04T10:12:30+00:00',
      sport: 'hockey',
      owner: 'henry',
      players: '10',
      game_id: 4
  },
  {
      notes: 'Game was played',
      time: '2017-10-14T20:34:30+00:00',
      sport: 'soccer',
      owner: 'john',
      players: '12',
      game_id: 5
  }
];

 extract() {
  var groups = {};

  this.data.forEach(function(val) {
      var date = val.time.split('T')[0];
      if (date in groups) {
          groups[date].push(val.sport);
      } else {
          groups[date] = new Array(val.sport);
      }
  });

  console.log(groups);
  return groups;
}



}
