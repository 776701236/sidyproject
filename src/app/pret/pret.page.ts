import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-pret',
  templateUrl: './pret.page.html',
  styleUrls: ['./pret.page.scss'],
})
export class PretPage implements OnInit {

  constructor(private publc:PublicService) { }

  result : any ;

  ngOnInit() {
    this.onLoadPret();
  }

  onLoadPret() {
    this.publc.getPret()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  
    },err=>{
      console.log(err); 
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.publc.getPret()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)      
    });

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

}
